#!/bin/bash

pip install --upgrade ansible ansible-core
ansible --version
ansible-community --version
pip install argcomplete
activate-global-python-argcomplete
ansible localhost -m ping